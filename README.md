
## Pre-requis
- Docker ([ici](https://docs.docker.com/engine/install/))
- Docker-compose ([ici](https://docs.docker.com/compose/install/))
- Git
- Gilab

## Lancement
**1-** Telecharger le projet [ici](https://gitlab.com/Itokiana/itokiana-scrapping-ruby/-/archive/main/itokiana-scrapping-ruby-main.zip) ou le cloner avec la commande suivante

    git clone https://gitlab.com/Itokiana/itokiana-scrapping-ruby.git

**2-** Ouvrir un terminal ou un cmd dans le repertoire du projet et lancer la commande suivante:

    docker-compose up

**3-** Ouvrir un navigateur et effectuer les requetes sur le lien suivant par exemple
[http://localhost:4567/foo/\[le-nom-de-rue\]](http://localhost:4567/foo/Rue+de+Lyon+59000+Lille)

**NB:** 
 - Il se peut que la reponse de la requete dure un peu plus longtemps en fonction du nombre de page que le script doit scrapper
 - La machine et la connexion aussi doivent etre assez performant pour accelerer le rendu
