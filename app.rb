require 'watir'
require 'pry'
require 'sinatra'
require 'json'
require 'rack/cors'

use Rack::Cors do
  allow do
    origins '*' # 'localhost:3000', 'localhost:8080', '0.0.0.0:3000', '127.0.0.1:3000',
    resource '*',  headers: :any, methods: [:get, :post, :delete, :put, :patch, :options, :head]
  end
end

# url = 'www.pagesjaunes.fr/pagesblanches/recherche?quoiqui=&ou=Rue+de+Lyon+59000+Lille&univers=pagesblanches&idOu='

def contains_number(str)
  str =~ /[0-9]/
end

def check_url(request)
  browser = Watir::Browser.new(:firefox)
  browser.goto 'https://www.pagesjaunes.fr/pagesblanches/recherche'
  
  browser.driver.manage.timeouts.implicit_wait = 5

  cookie_button = browser.span(text: 'Accepter & Fermer')
  cookie_button.click

  browser.driver.manage.timeouts.implicit_wait = 5
  input_search = browser.text_field(id: 'ou')
  button_search = browser.button(title: 'Trouver')

  input_search.set request
  button_search.click

  browser.driver.manage.timeouts.implicit_wait = 5
  check_adr_reperts = browser.divs(class: 'SEL-categorie')

  list_addr_rep = []
  results = []

  if(check_adr_reperts.length() > 0)
  
    check_adr_reperts.each{|div|
      if(div.h2.text == 'par "Adresse répertoriée"')
        div.ul.as.each{|a|
          list_addr_rep.push(a.title)
        }
      end
      p '='*100
      p list_addr_rep
      results = list_addr_rep
    }
  end
  browser.close
  return results
end

def get_results(search)
  browser = Watir::Browser.new(:firefox)
  url = 'https://www.pagesjaunes.fr/pagesblanches/recherche?ou=' + search
  browser.goto url
    
  browser.driver.manage.timeouts.implicit_wait = 5

  cookie_button = browser.span(text: 'Accepter & Fermer')
  cookie_button.click

  browser.driver.manage.timeouts.implicit_wait = 5

  results = []

  result_search = browser.lis(class: 'bi bi-generic')
  result_search2 = browser.lis(class: 'bi bi-health')
  result_search3 = browser.lis(class: 'bi bi-beauty')

  pagination = browser.span(id: 'SEL-compteur').text.split(' / ')[1].to_i
  page = 0


  while(page != pagination)
    browser.driver.manage.timeouts.implicit_wait = 5
    result_search3.each {|li|
      name = ''
      address = ''
      phonenumber = ''
      bloc_content = li.div(class: 'bi-clic-mobile').divs

      btn_phone = li.span(text: 'Afficher le N°')
      if(btn_phone.present?)
        btn_phone.click
      end

      browser.driver.manage.timeouts.implicit_wait = 3

      phonenumber = li.div(class: 'bi-ctas')
                      .div(class: 'bi-fantomas')
                      .div(class: 'number-contact txt_sm')
      if(phonenumber.present?)
        phone = phonenumber.text.split("\n").select {|i| contains_number(i) != nil }
      end
      

      if(bloc_content.length() == 2)
        name = bloc_content[1]
              .div(class: 'bi-content')
              .a(class: 'bi-denomination')
              .h3.text

        if(bloc_content[1]
          .div(class: 'bi-content')
          .div(class: 'bi-address small')
          .a(title: 'Voir le plan').present?)
          address = bloc_content[1]
                .div(class: 'bi-content')
                .div(class: 'bi-address small')
                .a(title: 'Voir le plan').text
        end
      else
        name = li
              .div(class: 'bi-clic-mobile')
              .a(class: 'bi-denomination')
              .h3.text
        if(li
          .div(class: 'bi-clic-mobile')
          .div(class: 'bi-address small')
          .a(title: 'Voir le plan').present?)
          address = li
                .div(class: 'bi-clic-mobile')
                .div(class: 'bi-address small')
                .a(title: 'Voir le plan').text
        end
      end
      
      results.push({
        :firstname => name.split(' ')[0], 
        :lastname => name.split(' ')[1], 
        :address => address.split(' Voir ')[0], 
        :phone => phone
      })
    }
    result_search2.each {|li|
      name = ''
      address = ''
      phonenumber = ''
      bloc_content = li.div(class: 'bi-clic-mobile').divs

      btn_phone = li.span(text: 'Afficher le N°')
      if(btn_phone.present?)
        btn_phone.click
      end

      browser.driver.manage.timeouts.implicit_wait = 3

      phonenumber = li.div(class: 'bi-ctas')
                      .div(class: 'bi-fantomas')
                      .div(class: 'number-contact txt_sm')
      if(phonenumber.present?)
        phone = phonenumber.text.split("\n").select {|i| contains_number(i) != nil }
      end
      

      if(bloc_content.length() == 2)
        name = bloc_content[1]
              .div(class: 'bi-content')
              .a(class: 'bi-denomination')
              .h3.text

        if(bloc_content[1]
          .div(class: 'bi-content')
          .div(class: 'bi-address small')
          .a(title: 'Voir le plan').present?)
          address = bloc_content[1]
                .div(class: 'bi-content')
                .div(class: 'bi-address small')
                .a(title: 'Voir le plan').text
        end
      else
        name = li
              .div(class: 'bi-clic-mobile')
              .a(class: 'bi-denomination')
              .h3.text
        if(li
          .div(class: 'bi-clic-mobile')
          .div(class: 'bi-address small')
          .a(title: 'Voir le plan').present?)
          address = li
                .div(class: 'bi-clic-mobile')
                .div(class: 'bi-address small')
                .a(title: 'Voir le plan').text
        end
      end
      
      results.push({
        :firstname => name.split(' ')[0], 
        :lastname => name.split(' ')[1], 
        :address => address.split(' Voir ')[0], 
        :phone => phone
      })
    }
    
    result_search.each {|li|
      name = ''
      address = ''
      phonenumber = ''
      bloc_content = li.div(class: 'bi-clic-mobile').divs

      btn_phone = li.span(text: 'Afficher le N°')
      if(btn_phone.present?)
        btn_phone.click
      end

      browser.driver.manage.timeouts.implicit_wait = 3

      phonenumber = li.div(class: 'bi-ctas')
                      .div(class: 'bi-fantomas')
                      .div(class: 'number-contact txt_sm')
      if(phonenumber.present?)
        phone = phonenumber.text.split("\n").select {|i| contains_number(i) != nil }
      end
      

      if(bloc_content.length() == 2)
        name = bloc_content[1]
              .div(class: 'bi-content')
              .a(class: 'bi-denomination')
              .h3.text

        if(bloc_content[1]
          .div(class: 'bi-content')
          .div(class: 'bi-address small')
          .a(title: 'Voir le plan').present?)
          address = bloc_content[1]
                .div(class: 'bi-content')
                .div(class: 'bi-address small')
                .a(title: 'Voir le plan').text
        end
      else
        name = li
              .div(class: 'bi-clic-mobile')
              .a(class: 'bi-denomination')
              .h3.text
        if(li
          .div(class: 'bi-clic-mobile')
          .div(class: 'bi-address small')
          .a(title: 'Voir le plan').present?)
          address = li
                .div(class: 'bi-clic-mobile')
                .div(class: 'bi-address small')
                .a(title: 'Voir le plan').text
        end
      end
      
      results.push({
        :firstname => name.split(' ')[0], 
        :lastname => name.split(' ')[1], 
        :address => address.split(' Voir ')[0], 
        :phone => phone
      })
    }

    page += 1
    if(pagination > 1)
      p '='*100
      p 'URL =>' + url
      p 'NAVIGATION DANS LA PAGE ' + page.to_s
      begin
        if(browser.a(id: 'pagination-next').present?)
          browser.a(id: 'pagination-next').click
        end
      rescue
        p "Next button not found"
      end
    end
  end
  browser.close

  return results
end

set :bind, '0.0.0.0'
set :port, 4567
set :foo, 'bar'
set :base_url, 'www.pagesjaunes.fr/pagesblanches/recherche?'

before do
  content_type :json
end

get '/foo/:search' do
  results = []
  begin
    check = check_url(params['search'])
    if(check.length() === 0)
      results = get_results(params['search'])
    else
      check.each{|addr|
        results += get_results(addr)
      }
    end
    return results.to_json
  rescue
    results.to_json
  end
end