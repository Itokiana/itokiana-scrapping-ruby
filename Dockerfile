FROM ruby:2.7.1-alpine3.10

ADD . /app_scrapping
WORKDIR /app_scrapping
RUN bundle install

EXPOSE 4567

CMD ["/bin/bash"]